const { findById } = require('../../models/bicicleta');
var Bicicleta = require('../../models/bicicleta');

beforeEach(()=> {
	Bicicleta.allBicis = [];
})

describe('Bicicleta.allBicis', () =>{
	it('comienza vacío', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
	});
});


describe('Bicicleta.add', ()=>{
	it('agregamos una', () => {
		// precondición
		expect(Bicicleta.allBicis.length).toBe(0);
		// some transformation to the data
		var a =  new Bicicleta(23, 'red', 'urbana', [0,0]);
		Bicicleta.add(a);
		// 
		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(a);
	})
})

describe('Bicicletas.findById', ()=> {
	it('Bicicletas.findById debe devolver la bici con id 1', ()=> {
		expect(Bicicleta.allBicis.length).toBe(0);
		var aBici =  new Bicicleta(1, 'negro', 'ruta');
		var aBici2 =  new Bicicleta(2, 'verde', 'gravel');
		Bicicleta.add(aBici);
		Bicicleta.add(aBici2);

		var targetBici = Bicicleta.findById(1);
		expect(targetBici.id).toBe(1);
		expect(targetBici.color).toBe(aBici.color);
		expect(targetBici.modelo).toBe(aBici.modelo);
	});
})

describe('Bicicleta.removeById',  () => {
	it('Bicicleta.removeById deberia devolver length 0', () =>{
		expect(Bicicleta.allBicis.length).toBe(0);
		var aBici = new Bicicleta(1, 'negro', 'ruta');
		Bicicleta.add(aBici);
		expect(Bicicleta.allBicis.length).toBe(1);
		Bicicleta.removeById(1);
		expect(Bicicleta.allBicis.length).toBe(0);



	})
})