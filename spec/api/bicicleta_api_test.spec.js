var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas con Mongo', () => {
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, "connection error"));
        db.once('open', function(){
            console.log("we are connected to testdbd");
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) {
                console.log(err);
            }
            done();
        });
    });

    describe('Bicicleta createInstance', () => {
        it('crea una instancia de la Bicicleta', ()=>{
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);

        })
    });

    describe('Bicicleta.allBicis', ()=>{
        it('comienza vacia', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            })
        })
    })

    describe('Bicicleta.add', ()=>{
        it('agregar solo una bici', (done)=>{
            var aBici = new Bicicleta({
                code: 1,
                color: "verde",
                modelo: "urbana"
            });
            Bicicleta.add(aBici, function(err, bicis){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(aBici.code);
                    
                    done();
                })
            });
        })
    })

    describe('Bicicleta.findByCode', ()=>{
        it('encontrar solo una bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){

                expect(bicis.length).toBe(0);
    
    
                var aBici = new Bicicleta({
                    code: 1,
                    color: "verde",
                    modelo: "urbana"
                });
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);
                    
                    var aBici2 = new Bicicleta({code:2, color:"rojo", modelo:"mountain"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
    
                        Bicicleta.findByCode(1, function(error, targetBici){
                            
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
    
                        })
                    });
    
                });
            })
        })
    })

})







/*
beforeEach(() => {
    Bicicleta.allBicis = []
});

describe('Bicicleta API', () => {
    describe('get BICICLETAS', () => {
        it('status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'negro', 'urbana');
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function (error, response, body) {
                expect(response.statusCode).toBe(200);
            }) //es necesario poner function, no funciona la flecha, y la llave tiene que estar pegada al parentesis
        })
    })

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}'; //es lo mismo que hacer el formato json y pasarlo a string (creo que eso se hace en el proyecto de angular)

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('rojo');
                done(); //es lo que espera jasmine para detener el test, algunas veces el test termina antes de que se realiza la peticion POST
            })
        });
    })
})
*/