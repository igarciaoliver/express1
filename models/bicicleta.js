var mongoose = require('mongoose');
var Schema =  mongoose.Schema;

var bicicletaSchema =  new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        // [Number] significa que es una array de numeros
        // ubicacion es una array de números,
        // cuyo  índice es un dato de tipo geográfico
        // hay muchos tipo de datos geograficos.... ni idea!
        type: [Number], index: { type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.methods.toString =  function() {
    return `code: ${this.code} color: ${this.color} modelo: ${this.modelo}`;
}
// .statis  es una método del modelo
bicicletaSchema.statics.createInstance = function(code, color,modelo, ubicacion) {
    return new this({
        code: code,
        color: color, 
        modelo: modelo,
        ubicacion: ubicacion 
    })
};

bicicletaSchema.statics.allBicis = function(cb) {
    // acá dentro this es el schema
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb) {
    // acá dentro this es el schema
    this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function (aCode, cb) {
    // acá dentro this es el schema
    this.findOne({code: aCode}, cb);
};

bicicletaSchema.statics.removeByCode = function (aCode, cb) {
    // acá dentro this es el schema
    this.deleteOne({code: aCode}, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema); 
