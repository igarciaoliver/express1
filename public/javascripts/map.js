
var map = L.map('main-map').setView([-34.5956009, -58.3946876], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([-34.5946876, -58.3946876]).addTo(map)
//     .bindPopup('Mi barrio.')
//     .openPopup();




$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        
        result.bicicletas.forEach( function(bici) {
            L.marker(bici.ubicacion, { title: bici.modelo }).addTo(map)
                .bindPopup(bici.modelo)
                .openPopup();

            
        });
    }
})